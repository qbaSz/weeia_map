﻿var scale = 5;
var color = 'orange';
var start_marker_icon = L.icon({
    iconUrl: '~/Content/Images/start-marker-icon.png',
    iconSize: [25, 41],
    iconAnchor: [12.5, 41],
    popupAnchor: [12.5, 10],
    shadowUrl: '~/Content/Images/marker-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [12.5, 41]
});

var end_marker_icon = L.icon({
    iconUrl: '~/Content/Images/end-marker-icon.png',
    iconSize: [25, 41],
    iconAnchor: [12.5, 41],
    popupAnchor: [12.5, 10],
    shadowUrl: '~/Content/Images/marker-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [12.5, 41]
});


function DrawPoint(map_x, map_y, point, map_path) {
    var img_lat = map_y / scale;
    var img_lng = map_x / scale;

    var map = L.map('mapid', {
        crs: L.CRS.Simple,
    }).setView([img_lat / 2, img_lng / 2], 1.5);

    var bounds = [[img_lat, 0], [0, img_lng]];

    var image = L.imageOverlay(map_path, bounds).addTo(map);

    var marker = L.marker([ptToLatLng(point.Y, img_lat), (point.X / scale)]).addTo(map);
    marker.bindPopup(point.Name).addTo(map);
}

function DrawMap(map_x, map_y, points, map_path) {
    var img_lat = map_y / scale;
    var img_lng = map_x / scale;

    var map = L.map('mapid', {
        crs: L.CRS.Simple,
    }).setView([img_lat / 2, img_lng / 2], 1.5);

    var bounds = [[img_lat, 0], [0, img_lng]];

    var image = L.imageOverlay(map_path, bounds).addTo(map);

    //map.setMaxBounds(bounds);
    var latlngPoints = new Array();

    for (i = 0; i < points.length; i++) {
        var pt_y = ptToLatLng(points[i].Y, img_lat);
        var pt_x = (points[i].X / scale);
        latlngPoints.push([pt_y, pt_x]);
    }

    var polyline = L.polyline(latlngPoints, { color: color, weight: 6 }).addTo(map);
    var marker_start = L.marker([ptToLatLng(points[0].Y, img_lat), (points[0].X / scale)]).addTo(map);
    var marker_end = L.marker([ptToLatLng(points[points.length - 1].Y, img_lat), (points[points.length - 1].X / scale)]).addTo(map);

    marker_start.bindPopup('<a href="ChangeMap?direction=0">Idź dalej</a>', { icon: start_marker_icon }).addTo(map);
    marker_end.bindPopup('<a href="ChangeMap?direction=1">Idź dalej</a>', { icon: end_marker_icon }).addTo(map);
}

function ptToLatLng(coord, map_y) {
    return (map_y - (coord / scale));
}