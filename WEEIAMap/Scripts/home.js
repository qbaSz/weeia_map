﻿function DrawMainMap() {
    var mymap = L.map('mapid').setView([51.752207, 19.453246], 17);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoicWJhc3oiLCJhIjoiY2o0aGFlMTN5MDFhZTJybzdqcTc2Z2xkNCJ9.58eJBOkt_g4qiPbtPGOQSw'
    }).addTo(mymap);
}


/*********************************************************
 * Depreciated functionality of drawing lines between divs
*********************************************************/
/*function connect(div1, div2, color, thickness) {
    var center1 = getCenterOfThePoint(div1);
    var center2 = getCenterOfThePoint(div2);
    // center1
    var x1 = center1.center_x;
    var y1 = center1.center_y;
    // center2
    var x2 = center2.center_x;
    var y2 = center2.center_y;
    // distance
    var length = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
    // center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    // angle
    var angle = Math.atan2((y1 - y2), (x1 - x2)) * (180 / Math.PI);
    // make hr
    var htmlLine = "<div style='padding:0px; margin:0px; height:" + thickness + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; z-index: 1000; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
    //
    $(".img-map").append(htmlLine);
}

function getCenterOfThePoint(element) {
    return {
        center_x: $(element).data("x") + $(element).height() / 2,
        center_y: $(element).data("y") + $(element).height() / 2
    }
}

window.drawLines = function () {
    var points = $(".dot");
    if (points.length > 0) {
        for (i = 1; i < points.length; i++) {
            connect(points[i - 1], points[i], "#F00", 3);
        }
    }
}*/