namespace WEEIAMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", "dbo.AdjecentLocations");
            DropIndex("dbo.AdjecentLocations", new[] { "AdjecentLocation_AdjecentLocationId" });
            AddColumn("dbo.AdjecentLocations", "Location_LocationId", c => c.Int());
            AddColumn("dbo.AdjecentLocations", "NextLocation_LocationId", c => c.Int());
            AddColumn("dbo.AdjecentLocations", "RootLocation_LocationId", c => c.Int());
            AddColumn("dbo.Locations", "Distance", c => c.Int(nullable: false));
            AddColumn("dbo.Locations", "IsVisited", c => c.Boolean(nullable: false));
            AddColumn("dbo.Locations", "ParentId", c => c.Int());
            AddColumn("dbo.Locations", "Parent_LocationId", c => c.Int());
            CreateIndex("dbo.AdjecentLocations", "Location_LocationId");
            CreateIndex("dbo.AdjecentLocations", "NextLocation_LocationId");
            CreateIndex("dbo.AdjecentLocations", "RootLocation_LocationId");
            CreateIndex("dbo.Locations", "Parent_LocationId");
            AddForeignKey("dbo.AdjecentLocations", "Location_LocationId", "dbo.Locations", "LocationId");
            AddForeignKey("dbo.Locations", "Parent_LocationId", "dbo.Locations", "LocationId");
            AddForeignKey("dbo.AdjecentLocations", "NextLocation_LocationId", "dbo.Locations", "LocationId");
            AddForeignKey("dbo.AdjecentLocations", "RootLocation_LocationId", "dbo.Locations", "LocationId");
            DropColumn("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", c => c.Int());
            DropForeignKey("dbo.AdjecentLocations", "RootLocation_LocationId", "dbo.Locations");
            DropForeignKey("dbo.AdjecentLocations", "NextLocation_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "Parent_LocationId", "dbo.Locations");
            DropForeignKey("dbo.AdjecentLocations", "Location_LocationId", "dbo.Locations");
            DropIndex("dbo.Locations", new[] { "Parent_LocationId" });
            DropIndex("dbo.AdjecentLocations", new[] { "RootLocation_LocationId" });
            DropIndex("dbo.AdjecentLocations", new[] { "NextLocation_LocationId" });
            DropIndex("dbo.AdjecentLocations", new[] { "Location_LocationId" });
            DropColumn("dbo.Locations", "Parent_LocationId");
            DropColumn("dbo.Locations", "ParentId");
            DropColumn("dbo.Locations", "IsVisited");
            DropColumn("dbo.Locations", "Distance");
            DropColumn("dbo.AdjecentLocations", "RootLocation_LocationId");
            DropColumn("dbo.AdjecentLocations", "NextLocation_LocationId");
            DropColumn("dbo.AdjecentLocations", "Location_LocationId");
            CreateIndex("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId");
            AddForeignKey("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", "dbo.AdjecentLocations", "AdjecentLocationId");
        }
    }
}
