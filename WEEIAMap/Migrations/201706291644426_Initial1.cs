namespace WEEIAMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Maps", "X", c => c.Int(nullable: false));
            AddColumn("dbo.Maps", "Y", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Maps", "Y");
            DropColumn("dbo.Maps", "X");
        }
    }
}
