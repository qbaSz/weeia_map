namespace WEEIAMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", c => c.Int());
            CreateIndex("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId");
            AddForeignKey("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", "dbo.AdjecentLocations", "AdjecentLocationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId", "dbo.AdjecentLocations");
            DropIndex("dbo.AdjecentLocations", new[] { "AdjecentLocation_AdjecentLocationId" });
            DropColumn("dbo.AdjecentLocations", "AdjecentLocation_AdjecentLocationId");
        }
    }
}
