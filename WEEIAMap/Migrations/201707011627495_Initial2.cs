namespace WEEIAMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjecentLocations",
                c => new
                    {
                        AdjecentLocationId = c.Int(nullable: false, identity: true),
                        RootLocationId = c.Int(nullable: false),
                        NextLocationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdjecentLocationId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdjecentLocations");
        }
    }
}
