namespace WEEIAMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjecentPoints",
                c => new
                    {
                        AdjecentPointId = c.Int(nullable: false, identity: true),
                        RootPointId = c.Int(nullable: false),
                        NextPointId = c.Int(nullable: false),
                        Point_PointId = c.Int(),
                        NextPoint_PointId = c.Int(),
                        RootPoint_PointId = c.Int(),
                    })
                .PrimaryKey(t => t.AdjecentPointId)
                .ForeignKey("dbo.Points", t => t.Point_PointId)
                .ForeignKey("dbo.Points", t => t.NextPoint_PointId)
                .ForeignKey("dbo.Points", t => t.RootPoint_PointId)
                .Index(t => t.Point_PointId)
                .Index(t => t.NextPoint_PointId)
                .Index(t => t.RootPoint_PointId);
            
            CreateTable(
                "dbo.Points",
                c => new
                    {
                        PointId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PositionTop = c.Int(nullable: false),
                        PositionLeft = c.Int(nullable: false),
                        ParentId = c.Int(),
                        IsVisited = c.Boolean(nullable: false),
                        Distance = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        MapId = c.Int(nullable: false),
                        Parent_PointId = c.Int(),
                    })
                .PrimaryKey(t => t.PointId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Maps", t => t.MapId, cascadeDelete: true)
                .ForeignKey("dbo.Points", t => t.Parent_PointId)
                .Index(t => t.LocationId)
                .Index(t => t.MapId)
                .Index(t => t.Parent_PointId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        InstituteName = c.String(),
                        FloorNum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LocationId);
            
            CreateTable(
                "dbo.Maps",
                c => new
                    {
                        MapId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ImagePath = c.String(),
                    })
                .PrimaryKey(t => t.MapId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdjecentPoints", "RootPoint_PointId", "dbo.Points");
            DropForeignKey("dbo.AdjecentPoints", "NextPoint_PointId", "dbo.Points");
            DropForeignKey("dbo.Points", "Parent_PointId", "dbo.Points");
            DropForeignKey("dbo.Points", "MapId", "dbo.Maps");
            DropForeignKey("dbo.Points", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.AdjecentPoints", "Point_PointId", "dbo.Points");
            DropIndex("dbo.Points", new[] { "Parent_PointId" });
            DropIndex("dbo.Points", new[] { "MapId" });
            DropIndex("dbo.Points", new[] { "LocationId" });
            DropIndex("dbo.AdjecentPoints", new[] { "RootPoint_PointId" });
            DropIndex("dbo.AdjecentPoints", new[] { "NextPoint_PointId" });
            DropIndex("dbo.AdjecentPoints", new[] { "Point_PointId" });
            DropTable("dbo.Maps");
            DropTable("dbo.Locations");
            DropTable("dbo.Points");
            DropTable("dbo.AdjecentPoints");
        }
    }
}
