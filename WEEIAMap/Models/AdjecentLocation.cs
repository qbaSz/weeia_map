﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class AdjecentLocation
    {
        public int AdjecentLocationId { get; set; }
        public int RootLocationId { get; set; }
        public int NextLocationId { get; set; }
        public Location RootLocation { get; set; }
        public Location NextLocation { get; set; }
    }
}