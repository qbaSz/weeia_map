﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models.Helpers
{
    public class PathPointsPackageModel
    {
        public List<Point> PathPoints { get; set; }
        public List<Location> PathLocations { get; set; }
    }
}