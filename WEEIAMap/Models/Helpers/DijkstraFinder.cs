﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEEIAMap.Models;

namespace WEEIAMap.Models.Helpers
{
    public class DijkstraFinder
    {
        DatabaseContext db = new DatabaseContext();

        public PathPointsPackageModel FindShortestPath(string classFrom, int locationFrom, string classTo, int locationTo)
        {
            var locationF = db.Locations.Where(l => l.LocationId == locationFrom).First();
            var mapF = db.Maps.Where(m => m.Name == (locationF.InstituteName + "_" + locationF.FloorNum + "f"));
            var locationsPath = FindLocationsPath(locationFrom, locationTo).ToList();
            var locations = locationsPath.Select(s => s.LocationId).ToList();
            var points = db.Points.Where(p => locations.Contains(p.LocationId)).ToList();
            List<Point> allPoints = new List<Point>();
            foreach (var pts in points)
            {
                allPoints.Add(new Point
                {
                    PointId = pts.PointId,
                    Distance = pts.Distance,
                    IsVisited = pts.IsVisited,
                    LocationId = pts.LocationId,
                    MapId = pts.MapId,
                    Name = pts.Name,
                    ParentId = null,
                    Parent = null,
                    PositionLeft = pts.PositionLeft,
                    PositionTop = pts.PositionTop,
                    AdjecentPoints = new List<AdjecentPoint>()
                });

            }
            foreach (var pts in allPoints)
            {
                var adjPts = db.AdjecentPoints.Where(a => a.RootPointId == pts.PointId).ToList();
                foreach (var adjPt in adjPts)
                {
                    var nextPoint = allPoints.Where(a => a.PointId == adjPt.NextPointId).FirstOrDefault();
                    if(nextPoint != null)
                    {
                        pts.AdjecentPoints.Add(new AdjecentPoint()
                        {
                            NextPoint = nextPoint
                        });
                    }
                }
            }
            var pt = allPoints.Where(p => p.Name == classFrom && p.LocationId == locationF.LocationId).First();
            pt.Distance = 0;
            while (pt != null)
            {
                pt.IsVisited = true;
                foreach (AdjecentPoint adjPoint in pt.AdjecentPoints)
                {
                    if (adjPoint.NextPoint.IsVisited) continue;

                    int distanceAdjParent = (int)Math.Sqrt(Math.Pow(adjPoint.NextPoint.PositionLeft - pt.PositionLeft, 2) + Math.Pow(adjPoint.NextPoint.PositionTop - pt.PositionTop, 2));
                    if (adjPoint.NextPoint.Distance > pt.Distance + distanceAdjParent)
                    {
                        adjPoint.NextPoint.Distance = pt.Distance + distanceAdjParent;
                        adjPoint.NextPoint.Parent = pt;
                        adjPoint.NextPoint.ParentId = pt.PointId;
                    }
                }

                pt = null;
                if (allPoints.Where(p => !p.IsVisited).Count() > 0)
                {
                    int min = allPoints.Where(p => !p.IsVisited).Min(m => m.Distance);
                    pt = allPoints.Where(p => !p.IsVisited && p.Distance == min).FirstOrDefault();
                }
            }

            Point end = allPoints.Where(p => p.Name == classTo).First();
            Point start = allPoints.Where(p => p.Name == classFrom).First();
            Point next = end.Parent;
            List<Point> finalPoints = new List<Point>();
            finalPoints.Add(end);
            while (next.PointId != start.PointId)
            {
                finalPoints.Add(next);
                next = next.Parent;
            }
            finalPoints.Add(next);
            finalPoints.Reverse();
            return new PathPointsPackageModel() { PathPoints = finalPoints, PathLocations = locationsPath };
        }

        public List<Location> FindLocationsPath(int locationFrom, int locationTo)
        {
            var locations = db.Locations.ToList();
            List<Location> allLocations = new List<Location>();
            List<Location> path = new List<Location>();
            foreach(var loc in locations)
            {
                allLocations.Add(new Location
                {
                    LocationId = loc.LocationId,
                    FloorNum = loc.FloorNum,
                    InstituteName = loc.InstituteName,
                    Distance = int.MaxValue,
                    IsVisited = false,
                    Parent = null,
                    ParentId = null,
                    AdjescentLocations = new List<AdjecentLocation>()
                });
            }
            foreach(var locs in allLocations)
            {
                var adjLoc = db.AdjecentLocations.Where(a => a.RootLocationId == locs.LocationId).ToList();
                foreach(var adjL in adjLoc)
                {
                    locs.AdjescentLocations.Add(new AdjecentLocation
                    {
                        AdjecentLocationId = adjL.AdjecentLocationId,
                        NextLocationId = adjL.NextLocationId,
                        NextLocation = allLocations.Where(a => a.LocationId == adjL.NextLocationId).First(),
                        RootLocationId = adjL.RootLocationId
                    });
                }

            }

            var location = allLocations.Where(p => p.LocationId == locationFrom).First();
            location.Distance = 0;
            while (location != null)
            {
                location.IsVisited = true;
                foreach (AdjecentLocation adjPoint in location.AdjescentLocations)
                {
                    if (adjPoint.NextLocation.IsVisited) continue;

                    int distanceAdjParent = 10;
                    if (adjPoint.NextLocation.Distance > location.Distance + distanceAdjParent)
                    {
                        adjPoint.NextLocation.Distance = location.Distance + distanceAdjParent;
                        adjPoint.NextLocation.Parent = location;
                        adjPoint.NextLocation.ParentId = location.LocationId;
                    }
                }

                location = null;
                if (allLocations.Where(p => !p.IsVisited).Count() > 0)
                {
                    int min = allLocations.Where(p => !p.IsVisited).Min(m => m.Distance);
                    location = allLocations.Where(p => !p.IsVisited && p.Distance == min).FirstOrDefault();
                }
            }

            Location end = allLocations.Where(p => p.LocationId == locationTo).First();
            Location start = allLocations.Where(p => p.LocationId == locationFrom).First();
            Location next = end.Parent;
            path.Add(new Location { InstituteName = end.InstituteName, FloorNum = end.FloorNum, LocationId = end.LocationId });
            while (next != null)
            {
                path.Add(new Location { InstituteName = next.InstituteName, FloorNum = next.FloorNum, LocationId = next.LocationId });
                next = next.Parent;
            }
            path.Reverse();
            return path;
        }
    }
}