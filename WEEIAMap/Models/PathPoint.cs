﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class PathPoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
    }
}