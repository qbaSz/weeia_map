﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class Point
    {
        public int PointId { get; set; }
        public string Name { get; set; }
        public int PositionTop { get; set; }
        public int PositionLeft { get; set; }
        public int? ParentId { get; set; }
        public virtual Point Parent { get; set; }
        public bool IsVisited { get; set; }
        public int Distance { get; set; }
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }
        public int MapId { get; set; }
        public virtual Map Map { get; set; }
        public virtual ICollection<AdjecentPoint> AdjecentPoints { get; set; }
    }
}