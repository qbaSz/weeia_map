﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("DefaultConnection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Point> Points { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<AdjecentPoint> AdjecentPoints { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<AdjecentLocation> AdjecentLocations { get; set; }
    }
}