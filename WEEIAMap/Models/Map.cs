﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class Map
    {
        public int MapId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public virtual ICollection<Point> Points { get; set; }
    }
}