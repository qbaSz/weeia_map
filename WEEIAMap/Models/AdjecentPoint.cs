﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class AdjecentPoint
    {
        public int AdjecentPointId { get; set; }
        public int RootPointId { get; set; }
        public int NextPointId { get; set; }
        public virtual Point RootPoint { get; set; }
        public virtual Point NextPoint { get; set; }
    }
}