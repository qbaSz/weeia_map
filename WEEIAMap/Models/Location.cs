﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEEIAMap.Models
{
    public class Location
    {
        public int LocationId { get; set; }
        public string InstituteName { get; set; }
        public int FloorNum { get; set; }
        public int Distance { get; set; }
        public bool IsVisited { get; set; }
        public int? ParentId { get; set; }
        public Location Parent { get; set; }
        public virtual ICollection<Point> Points { get; set; }
        public virtual ICollection<AdjecentLocation> AdjescentLocations { get; set; }
    }
}