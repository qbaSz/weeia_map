﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WEEIAMap.Models;
using WEEIAMap.Models.Helpers;

namespace WEEIAMap.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        DijkstraFinder DFinder = new DijkstraFinder();
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string classNoFrom, int locationFrom, string classNoTo, int locationTo)
        {
            return RedirectToAction("MapDemo", new { classNoFrom = classNoFrom, locationFrom = locationFrom, classNoTo = classNoTo, locationTo = locationTo });
        }

        [HttpGet]
        public ActionResult MapDemo(string classNoFrom, int locationFrom, string classNoTo, int locationTo)
        {
            if(classNoTo == "" || classNoFrom == "")
            {
                return View("Index");
            }
            if(classNoFrom == classNoTo && locationTo == locationFrom)
            {
                var point = db.Points.Where(p => p.Name == classNoFrom && p.LocationId == locationFrom).First();
                var mapa = db.Maps.Where(m => m.Name == (point.Location.InstituteName + "_" + point.Location.FloorNum + "f")).First();
                ViewData["Map"] = mapa;
                return View("Point", new PathPoint() { X = point.PositionLeft, Y = point.PositionTop, Name = point.Name });
            }
            PathPointsPackageModel pckg = DFinder.FindShortestPath(classNoFrom, locationFrom, classNoTo, locationTo);
            List<KeyValuePair<Map, List<PathPoint>>> mainList = new List<KeyValuePair<Map, List<PathPoint>>>();
            foreach(var location in pckg.PathLocations)
            {
                List<PathPoint> tempPathList = new List<PathPoint>();
                List<Point> points = pckg.PathPoints.Where(p => p.LocationId == location.LocationId).ToList();
                Map tempMap = db.Maps.Where(m => m.Name == (location.InstituteName + "_" + location.FloorNum + "f")).First();
                foreach (var point in points)
                {
                    tempPathList.Add(new PathPoint
                    {
                        X = point.PositionLeft,
                        Y = point.PositionTop,
                        Name = point.Name
                    });
                }
                mainList.Add(new KeyValuePair<Map, List<PathPoint>>(tempMap, tempPathList));
            }
            Session["MainList"] = mainList;
            Session["Index"] = 0;
            var map = mainList.First().Key;
            ViewData["Map"] = map;
            return View("MapDemo",mainList.First().Value);
        }

        [HttpGet]
        public ActionResult ChangeMap(int direction)
        {
            List<PathPoint> pathPoints;
            int index = (int)Session["Index"];
            var mainList = (List<KeyValuePair<Map, List<PathPoint>>>)Session["MainList"];
            if (direction == 1)
            {
                if(mainList.Count >= index + 1) index++;
            }
            else if(direction == 0)
            {
                if(index - 1 >= 0) index--;
            }
            ViewData["Map"] = mainList[index].Key;
            pathPoints = mainList[index].Value;
            return View("MapDemo", pathPoints);
        }

        public ActionResult BuildNavbar()
        {
            var locations = db.Locations.ToList();
            return PartialView(locations);
        }

        public ActionResult FillClassNames(int locationId)
        {
            var points = db.Points.Where(p => p.LocationId == locationId && p.Name != "--").ToList();
            return PartialView(points);
        }
    }
}